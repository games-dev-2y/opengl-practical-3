#include "Game.h"

Game::Game(ContextSettings settings) :
	window(VideoMode(800, 600), "OpenGL", Style::Default, settings)
{
}

Game::~Game()
{
}

void Game::run()
{

	initialize();

	Event event;

	while (isRunning){

		cout << "Game running..." << endl;

		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
			{
				isRunning = false;
			}
		}
		update();
		draw();
	}

}

void Game::initialize()
{
	isRunning = true;
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, window.getSize().x / window.getSize().y, 1.0, 500.0);
	glMatrixMode(GL_MODELVIEW);
}

void Game::update()
{
	cout << "Update up" << endl;
}

void Game::draw()
{
	cout << "Draw up" << endl;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawPoint();
	drawLine();
	drawLineStrip();
	drawLineLoop();
	drawTriangle();
	drawTriangleStrip();
	drawTriangleFan();
	drawQuad();
	drawQuadStrip();
	drawPolygon();

	window.display();
}

void Game::drawPoint()
{
	vector<Vector3> pointPos
	{
		Vector3({ 2.0f, 2.0f, -5.0f }),
		Vector3({ 2.0f, 1.9f, -5.0f })
	};
	GLfloat pointSize = 10.0f;
	draw(GL_POINTS, pointPos, pointSize);
}

void Game::drawLine()
{
	vector<Vector3> linePos
	{
		Vector3({ -1.0f, 2.0f, -5.0f }),
		Vector3({ 0.0f, 2.0f, -5.0f })
	};
	draw(GL_LINES, linePos);
}

void Game::drawLineStrip()
{
	vector<Vector3> lineStripPos
	{
		Vector3({ -2.0f, 2.0f, -5.0f }),
		Vector3({ -1.8f, 2.0f, -5.0f }),
		Vector3({ -1.8f, 1.8f, -5.0f })
	};
	draw(GL_LINE_STRIP, lineStripPos);
}

void Game::drawLineLoop()
{
	vector<Vector3> lineLoopPos
	{
		Vector3({ -2.0f, 1.7f, -5.0f }),
		Vector3({ -1.8f, 1.7f, -5.0f }),
		Vector3({ -1.8f, 1.5f, -5.0f }),
		Vector3({ -2.0f, 1.5f, -5.0f })
	};
	draw(GL_LINE_LOOP, lineLoopPos);
}

void Game::drawTriangle()
{
	std::vector<Vector3> trianglePos
	{
		Vector3({ -1.7f, 2.0f, -5.0f }),
		Vector3({ -1.5f, 2.0f, -5.0f }),
		Vector3({ -1.5f, 1.8f, -5.0f })
	};
	draw(GL_TRIANGLES, trianglePos);
}

void Game::drawTriangleStrip()
{
	std::vector<Vector3> triangleStripPos
	{
		Vector3({ -1.7f, 1.7f, -5.0f }),
		Vector3({ -1.5f, 1.7f, -5.0f }),
		Vector3({ -1.7f, 1.5f, -5.0f }),
		Vector3({ -1.4f, 1.5f, -5.0f })
	};
	draw(GL_TRIANGLE_STRIP, triangleStripPos);
}

void Game::drawTriangleFan()
{
	vector<Vector3> triangleFanPos
	{
		Vector3({ -1.7f, 1.4f, -5.0f }),
		Vector3({ -1.5f, 1.4f, -5.0f }),
		Vector3({ -1.5f, 1.2f, -5.0f }),
		Vector3({ -1.8f, 1.2f, -5.0f })
	};
	draw(GL_TRIANGLE_FAN, triangleFanPos);
}

void Game::drawQuad()
{
	std::vector<Vector3> quadPos
	{
		Vector3({ -0.9f, 1.9f, -5.0f }),
		Vector3({ -0.3f, 1.9f, -5.0f }),
		Vector3({ -0.3f, 1.7f, -5.0f }),
		Vector3({ -0.9f, 1.7f, -5.0f })
	};
	draw(GL_QUADS, quadPos);
}

void Game::drawQuadStrip()
{
	vector<Vector3> quadStripPos
	{
		Vector3({ 0.5f, 1.9f, -5.0f }),
		Vector3({ 0.5f, 1.7f, -5.0f }),
		Vector3({ 0.7f, 1.7f, -5.0f }),
		Vector3({ 0.7f, 1.5f, -5.0f }),
		Vector3({ 0.9f, 1.7f, -5.0f }),
		Vector3({ 0.9f, 1.5f, -5.0f })
	};
	draw(GL_QUAD_STRIP, quadStripPos);
}

void Game::drawPolygon()
{
	vector<Vector3> polyPos
	{
		Vector3({ 0.0f, 0.2f, -5.0f }),
		Vector3({ 0.2f, 0.1f, -5.0f }),
		Vector3({ 0.1f, -0.1f, -5.0f }),
		Vector3({ -0.1f, -0.1f, -5.0f }),
		Vector3({ -0.2f, 0.1f, -5.0f })
	};
	draw(GL_POLYGON, polyPos);
}

/// <summary>
/// @brief Draws in OpenGL the passed shape and vector of positions
/// </summary>
/// <param name = "shape"> OpenGL shape </param>
/// <param name = "pos"> vector container for a 3d vector of positions </param>
/// <param name = "size"> float defining the size of the points </param>
/// <param name = "colour"> defines the colour of the shape </param>
void Game::draw(GLenum shape, std::vector<Vector3> pos, GLfloat size, Vector3 colour)
{
	glPointSize(size);
	glBegin(shape);
	{
		glColor3f(colour[0], colour[1], colour[2]);
		for (auto & point : pos)
		{
			glVertex3d(point[0], point[1], point[2]);
		}
	}
	glEnd();
}
