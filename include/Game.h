#include <iostream>
#include "SFML\Window.hpp"
#include "SFML\OpenGL.hpp"
/// <summary>
/// This hooks up OpenGL with our Game
/// </summary>
#include <gl\GL.h>
#include <gl\GLU.h>

using namespace std;
using namespace sf;

class Game
{
	typedef std::vector<GLfloat> Vector3;
public:
	Game(ContextSettings settings = ContextSettings());
	~Game();
	void run();
private:
	Window window;
	bool isRunning = false;
	void initialize();
	void update();
	void draw();

	void drawPoint();
	void drawLine();
	void drawLineStrip();
	void drawLineLoop();
	void drawTriangle();
	void drawTriangleStrip();
	void drawTriangleFan();
	void drawQuad();
	void drawQuadStrip();
	void drawPolygon();

	void draw(GLenum shape, std::vector<Vector3> pos, GLfloat size = 1.0f, Vector3 colour = Vector3({ 1.0f, 1.0f, 1.0f }));
};